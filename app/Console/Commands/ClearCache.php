<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class ClearCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:clear-cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear app cache';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $commands = ['config:clear','config:cache','route:clear','view:clear','cache:clear'];
        foreach ($commands as $command){
            $artisan = Artisan::call($command);
            $output = Artisan::output();
            dump($output);
        }
    }
}
