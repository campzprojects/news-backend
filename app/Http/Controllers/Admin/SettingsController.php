<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Gate;

class SettingsController extends Controller
{
   public function index()
   {
       // return view('auth.change_password');
   }

   public function save(Request $request)
   {
       # code...
   }

   public function storageLink(){
       $commands = ['storage:link','config:clear','config:cache','route:clear','view:clear','cache:clear'];
       foreach ($commands as $command){
           $artisan = Artisan::call($command);
           $output = Artisan::output();
           dump($output);
       }
   }
}
