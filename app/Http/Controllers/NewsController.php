<?php

namespace App\Http\Controllers;

use App\Media;
use App\News;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class NewsController extends Controller
{
    public function index()
    {
        $news = News::getNews();
        return view('news.index')
            ->with('news',$news);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'title' => 'required',
                'description' => 'required',
            ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $data = [
            'title' => $request->title,
            'description' => $request->description,
            'comment' => $request->comment,
            'created_by' => 1,
            'created_at' => Carbon::now()
        ];
        $savedNews = News::storeNews($data);
        $this->storeMedia($request, $savedNews->id);

        if ($savedNews){
            return response()->json([
                "success" => true,
                "message" => "News Uploaded Successfully!",
            ]);
        }

    }

    private function storeMedia($request,$newsId)
    {
        Log::info(json_encode($request->all()));
        Log::info(json_encode($request->file('files')));
//        if($request->file('files') && count($request->file('files'))){


            foreach ($request->file('files') as $file){
//        for ($x = 1; $x < 10; $x++) {
//            if ($request->hasFile('file_'.$x)){
//                $file = $request->file('file_'.$x);
                $ext = $file->extension();
                $path = 'media';
                $file_name = time() . rand(10000, 100000) . $file->extension() . '.' . $ext; //create file name
                $file->move('./' . $path, $file_name); //path to save file
                $file_path = $path . '/' . $file_name;
                $file_type = 'image';

                $data = [
                    'news_id' => $newsId,
                    'file_name' => $file_name,
                    'file_path' => $file_path,
                    'file_type' => $file_type,
                    'file_ext' => $ext,
                    'created_by' => 1,
                    'created_at' => Carbon::now()
                ];

                Media::storeMedia($data);
            }
//        }
//            }
//        }
    }

    public function downloadZip($id){
        $zip = new \ZipArchive();
        $news = News::getOne($id);
        $newsTitle = preg_replace('/[^A-Za-z0-9\-]/', '', $news->title).'-'.Carbon::parse($news->created_at)->format('Y-m-d').'-'.rand(100,1000);
        $zip_file = $newsTitle.'.zip';

        if ($zip->open($zip_file, \ZipArchive::CREATE | \ZipArchive::OVERWRITE) !== TRUE) {
            echo 'Could not open ZIP file.';
            return;
        }

        $mediaList = Media::getMedia($id)->pluck('file_path')->toArray();

        $newsContent = '';
        $newsContent .= "##News Title: \n\n";
        $newsContent .= $news->title . "\n\n";
        $newsContent .= "##News Description: \n\n";
        $newsContent .= $news->description . "\n\n";
        $newsContent .= "##News Comments: \n\n";
        $newsContent .= $news->comment . "\n\n";
        $newsContent .= "##News Media: \n\n";
        $newsContent .= count($mediaList) . " (attached ZIP file)\n\n";

        $txtFile = Storage::disk('public')->put('contents/'.$newsTitle.'.txt', $newsContent);
        $txtFilePath = 'storage/contents/'.$newsTitle.'.txt';

        // Add File in ZipArchive
        if (count($mediaList)){

            foreach($mediaList as $file)
            {
                if (! $zip->addFile($file, basename($file))) {
                    echo 'Could not add file to ZIP: ' . $file;
                }
            }

        }

        if ($txtFile){
            $zip->addFile($txtFilePath, $newsTitle.'.txt');
        }
        // Close ZipArchive
        $zip->close();

        return response()->download($zip_file);
//        rename($tempFileUri, 'zipPathAndName');
    }

    public function download()
    {
        return view('download');
    }

    public function view($id)
    {
        $news = News::getOne($id);
        return view('news.view')
            ->with('news', $news);
    }

    public function delete($id)
    {
        $news = News::deleteNews($id);
        if ($news) {
            Media::deleteMedia($id);
            session()->flash('message', "News Deleted Successfully!");

        } else {
            session()->flash('error', "something went wrong!");
        }

        return redirect()->route('admin.news.index');
    }
}
