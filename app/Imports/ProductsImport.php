<?php

namespace App\Imports;

use App\Product;
use App\Products;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ProductsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Product([
            'supplier_code'     => trim($row['supplier_code']),
            'product_id'    => trim($row['product_id']),
            'product_name'    => $row['product_name'],
            'stock_count'    => trim($row['stock_count']),
        ]);
    }
}
