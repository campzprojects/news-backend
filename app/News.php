<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    protected $guarded = [];

    public function media()
    {
        return $this->hasMany(Media::class, 'news_id', 'id');
    }

    public static function storeNews($data)
    {
        $news = News::create($data);

        return $news;
    }

    public static function getNews($perPage = false)
    {
        $news = News::with(['media']);
        if ($perPage) {
            $res = $news->paginate($perPage);
        } else {
            $res = $news->orderBy('created_at','desc')->get();
        }
        return $res;
    }

    public static function getOne($id){
        return News::where('id',$id)->with(['media'])->first();
    }
    public static function deleteNews($id){
        return News::where('id',$id)->delete();
    }
}
