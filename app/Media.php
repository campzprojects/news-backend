<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'media';
    protected $guarded = [];

    public static function storeMedia($data)
    {
        $media = Media::create($data);

        return $media;
    }

    public static function getMedia($id = null)
    {
        if ($id) {
            return Media::where('news_id', $id)->get();
        }
    }

    public static function deleteMedia($id)
    {
        return Media::where('news_id', $id)->delete();
    }
}
