<?php

Route::group(['prefix' => '/v1', 'namespace' => 'Api\V1', 'as' => 'api.'], function () {

});

Route::post('register',[\App\Http\Controllers\passportAuthController::class,'registerUserExample']);
Route::post('login',[\App\Http\Controllers\passportAuthController::class,'loginUserExample']);
//add this middleware to ensure that every request is authenticated
Route::middleware('auth:api')->group(function(){
    Route::get('user', [\App\Http\Controllers\passportAuthController::class,'authenticatedUserDetails']);
});

Route::post('news/store','NewsController@store');