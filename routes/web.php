<?php

Route::redirect('/', 'admin/home');

Auth::routes(['register' => false]);

Route::group(['middleware' => ['checkstatus']], function () {
    Route::post('/login', 'Auth\LoginController@login');
});

// Change Password Routes...
Route::get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
Route::patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/news', 'NewsController@index')->name('news.index');
    Route::get('/news/{id}', 'NewsController@view')->name('news.view');
    Route::get('/delete/{id}', 'NewsController@delete')->name('news.delete');
    Route::get('/download/zip/{id}', 'NewsController@downloadZip')->name('news.downloadZip');

    
    // settings
    Route::get('settings/general', 'Admin\SettingsController@index')->name('settings.general');
    Route::get('storage/link', 'Admin\SettingsController@storageLink')->name('storage.link');
    Route::post('settings/general/save', 'Admin\SettingsController@save')->name('settings.general.save');

});

Route::get('download', 'NewsController@download')->name('app.download');