@extends('layouts.admin')

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <h2>Dashboard</h2>
            </div>
        </div>
        {{--    admin dashboard--}}
        @can('admin_dashboard')
            <br>
            <div class="row">

                <div class="col-6 col-sm-4 col-md-3">
                    <a href="{{ route("admin.news.index") }}"
                       class="">
                        <div class="card mn200 ybox ">
                            <div class="media">
                                <p class="dashnumber">
                                    <ion-icon name="list-outline"></ion-icon>
                                </p>
                                <div class="media-body">
                                    <h5 class="dashhead">Manage News</h5>

                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        @endcan
        {{--   END admin dashboard--}}


    </div>
@endsection
@section('scripts')
    @parent

@endsection