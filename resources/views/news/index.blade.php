@extends('layouts.admin')
@section('content')
    <div class="card">
        <div class="card-header">
            Manage News
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                    <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            News Title
                        </th>
                        <th>
                            News Description
                        </th>
                        <th>
                            Comment
                        </th>
                        <th>
                            Media
                        </th>
                        <th>
                            Reported At
                        </th>
                        <th>
                            Download Zip
                        </th>

                    </tr>
                    </thead>
                    <tbody>
                    @if(count($news))
                        @foreach($news as $newsSingle)
                            <tr>
                                <td></td>
                                <td>{{$newsSingle->title}}</td>
                                <td>{{\Illuminate\Support\Str::limit($newsSingle->description,50,' ...')}}</td>
                                <td>{{$newsSingle->comment}}</td>
                                <td>
                                    @if(count($newsSingle->media))
                                        <div>
                                            @foreach($newsSingle->media as $media)
                                                <img src="{{asset($media->file_path)}}" alt="" width="50px">
                                            @endforeach
                                        </div>
                                    @else
                                        <b>-</b>
                                    @endif
                                </td>
                                <td>{{\Carbon\Carbon::parse($newsSingle->created_at)->diffForHumans()}}</td>
                                <td>
                                    <a href="{{ route('admin.news.view', $newsSingle->id) }}" class="btn btn-primary"
                                       title="View News">
                                        <i class="nav-icon fas fa-fw fa-eye">
                                        </i>
                                    </a>
                                    <a class="" title="Download as Zip"
                                       href="{{ route('admin.news.downloadZip', $newsSingle->id) }}">
                                        <img src="{{asset('images/zip.png')}}" width="20%">
                                    </a>
                                    <a href="{{ route('admin.news.delete', $newsSingle->id) }}" class="btn btn-primary"
                                       title="Delete News">
                                        <i class="nav-icon fas fa-fw fa-trash">
                                        </i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>


        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script>
        $(function () {
            let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

            $.extend(true, $.fn.dataTable.defaults, {
                // order: [[5, 'desc']],
                pageLength: 100,
            });
            $('.datatable-User:not(.ajaxTable)').DataTable({buttons: dtButtons})
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust();
            });
        });

    </script>
@endsection