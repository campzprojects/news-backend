@extends('layouts.admin')
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-10">
                    <h3 class="w-75">View News</h3>
                </div>
                <div class="col-md-2 text-right">
                    <a href="{{ route('admin.news.downloadZip', $news->id) }}" class="btn btn-info"
                       title="Download as Zip">ZIP</a>
                    <a href="{{ route('admin.news.delete', $news->id) }}" class="btn btn-danger"
                       style="margin-left: 5px" title="Delete News">Delete</a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h5>News Title</h5>
                            <p>{{$news->title}}</p>
                            <br>
                            <h5>News Description</h5>
                            <p>{{$news->description}}</p>
                            <br>
                            <h5>News Comment</h5>
                            <p>{{$news->comment}}</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h5>News Media</h5>
                            @if(count($news->media))
                                @foreach($news->media as $media)
                                    <img src="{{asset($media->file_path)}}" alt="" width="25%" style="
                                    padding: 8px;
                                    box-shadow: 0px 0px 13px 0px #999;
                                    border-radius: 10px;
                                ">

                                @endforeach
                            @else
                                <p class="text-center" style="background:#eee">
                                    No image found
                                </p>
                            @endif

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('scripts')
    @parent

@endsection