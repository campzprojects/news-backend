@extends('layouts.admin')
@section('content')
<div class="row">
<div class="col-md-8">
    <div class="card">
        <div class="card-header">
            {{ trans('global.create') }} Supplier
        </div>

    <div class="card-body">
        <form action="{{ route("admin.users.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('supplier_code') ? 'has-error' : '' }}">
                <label for="name">Supplier Code *</label>
                <input type="text" id="supplier_code" name="supplier_code" class="form-control"
                       value="{{ old('supplier_code', isset($user) ? $user->supplier_code : '') }}" required>
                @if($errors->has('supplier_code'))
                    <em class="invalid-feedback">
                        {{ $errors->first('supplier_code') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">{{ trans('cruds.user.fields.name') }}*</label>
                <input type="text" id="name" name="name" class="form-control"
                       value="{{ old('name', isset($user) ? $user->name : '') }}" required>
                @if($errors->has('name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                <label for="email">{{ trans('cruds.user.fields.email') }}*</label>
                <input type="email" id="email" name="email" class="form-control"
                       value="{{ old('email', isset($user) ? $user->email : '') }}" required>
                @if($errors->has('email'))
                    <em class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.email_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                <label for="password">{{ trans('cruds.user.fields.password') }}</label>
                <input type="password" id="password" name="password" class="form-control" required>
                @if($errors->has('password'))
                    <em class="invalid-feedback">
                        {{ $errors->first('password') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.password_helper') }}
                </p>
            </div>
            @foreach($roles as $id => $roles)
                @if( $roles == 'supplier')
                    <input type="hidden" value="{{$id}}" name="roles[]">
                @endif
            @endforeach

            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
    </div>
    
</div>
<div class="col-md-4">
    @include('admin.users.password_gen')
</div>
</div>
@endsection